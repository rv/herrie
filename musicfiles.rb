class Musicfiles

  def initialize
    @log = Logging.logger.root
  end

  def scan
    mp3_files = Dir.glob("*.mp3", File::FNM_CASEFOLD)
    return mp3_files.sort
  end

end
class Scanner

  require './storagedirs.rb'
  require './musicdirs.rb'

  def initialize db
    @db = db
    @log = Logging.logger.root
    @storage_dirs = StorageDirs.new
  end

  def add_storage_dir dir
    @storage_dirs.add dir
  end

  def scan
    @storage_dirs.each do |storage_dir|
      music_dirs = MusicDirs.new @db, storage_dir
      music_dirs.scan
    end
  end

end

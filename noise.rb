module Noise
  def Noise.init
    require 'logging'
    Dir.mkdir('./log') unless Dir.exist?('./log')
    Logging.color_scheme( 'bright',
                          :levels => {
                              :debug => :magenta,
                              :info  => :green,
                              :warn  => :yellow,
                              :error => :red,
                              :fatal => [:white, :on_red]
                          },
                          :date => :blue,
                          :logger => :cyan,
                          :message => :white
    )
    Logging.logger.root.level = :debug
    Logging.logger.root.appenders =
        Logging.appenders.stdout(
            'stdout',
            :layout => Logging.layouts.pattern(:pattern => '[%d] %5l %m\n',
                                               :color_scheme => 'bright' #'default'
            )
        ),
        Logging.appenders.rolling_file(
            './log/herrie.log',
            :layout => Logging.layouts.pattern(:pattern => '[%d] %5l %m\n'),
            :age => 'daily',
            :roll_by => 'date'
        )
    Logging.logger.root
  end
end
#!/usr/bin/env ruby
# -*- coding: utf-8 -*-
VERSION = 0.3

require './noise.rb'
require './scanner.rb'
require './database.rb'

log = Noise.init
log.info ""
log.info "\u{266b}" * 20
log.info "Herrie #{VERSION}"
log.info "\u{266b}" * 20
log.info ""

db = Database.new

scanner = Scanner.new db
scanner.add_storage_dir '/home/rene/Downloads/herrie/store-1'
scanner.add_storage_dir '/home/rene/Downloads/herrie/store-2'
scanner.add_storage_dir '/home/rene/Downloads/herrie/store-3'
scanner.scan


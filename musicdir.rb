class MusicDir

  require './coverfile.rb'
  require './musicfiles.rb'
  require './mp3tags.rb'
  require './database.rb'

  def initialize db, storagedir, storagedir_db, musicdir
    @db = db
    @log = Logging.logger.root
    @storagedir = storagedir
    @storagedir_db = storagedir_db
    @musicdir = musicdir
  end

  def scan
    @log.info ""
    @log.info "\u{1f5c1} Scanning #{@musicdir}"
    dir = File.join(@storagedir, @musicdir)
    Dir.chdir(dir)
    findcover
    findmusic
    unless @music.empty?
      musicmeta
      storemusic
    end
  end

  def findcover
    coverfile = Coverfile.new
    @cover = coverfile.scan
  end

  def findmusic
    musicfiles = Musicfiles.new
    @music = musicfiles.scan
    if @music.empty?
      @log.error 'No mp3 files found'
    end
  end

  def musicmeta
    mp3tags = MP3tags.new @music
    @tags = mp3tags.scan
  end

  def storemusic
    info = {}
    info[:storagedir] = @storagedir
    info[:storagedir_db] = @storagedir_db
    info[:musicdir] = @musicdir
    info[:cover] = @cover
    info[:music] = @music
    info[:tags] = @tags
    @db.add_record info
  end

end

require 'active_record'

class Storagedir < ActiveRecord::Base
  has_many :records
end

class Record < ActiveRecord::Base
  belongs_to :storagedir
  has_many :tracks
end

class Track < ActiveRecord::Base
  belongs_to :record
end

class Database

  def initialize
    @log = Logging.logger.root
    connect
    schema
  end

  def connect
    File.delete('_herrie.db') if File.exist?('_herrie.db')
    ActiveRecord::Base.establish_connection(
      adapter: 'sqlite3',
      database: '_herrie.db'
    )
  end

  def schema
    ActiveRecord::Schema.define do
      create_table :storagedirs do |table|
        table.column :dir, :string
      end
      create_table :records do |table|
        table.column :artist, :string
        table.column :album, :string
        table.column :year, :integer
        table.column :genre, :string
        table.column :cover, :string
        table.column :dir, :string
        table.column :storagedir_id, :integer
      end
      create_table :tracks do |table|
        table.column :track, :integer
        table.column :title, :string
        table.column :fname, :string
        table.column :length, :integer
        table.column :record_id, :integer
      end
    end
  end

  def add_storagedir dir
    Storagedir.create(dir: dir)
  end

  def add_record info
    record = info[:storagedir_db].records.create(
      dir: info[:musicdir],
      cover: info[:cover],
      artist: info[:tags][0][:artist],
      album: info[:tags][0][:album],
      year: info[:tags][0][:year],
      genre: info[:tags][0][:genre]
    )
    add_tracks record, info[:tags]
  end

  def add_tracks record, tags
    tags.each do |tag|
      record.tracks.create(
        track: tag[:track],
        title: tag[:title],
        fname: tag[:fname],
        length: tag[:length]
      )
    end
  end

end

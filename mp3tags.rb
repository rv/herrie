class MP3tags

  require 'taglib'

  def initialize files
    @log = Logging.logger.root
    @files = files
    @tags = []
  end

  def scan
    @files.each do |file|
      @tags.push meta file
    end
    return @tags
  end

  def meta file
    tags = {}
    tags[:fname] = file
    fileref = TagLib::FileRef.open(file) do |fileref|
      if not fileref.null?
        tag = fileref.tag
        tags[:title] = tag.title
        tags[:artist] = tag.artist
        tags[:album] = tag.album
        tags[:year] = tag.year
        tags[:track] = tag.track
        tags[:genre] = tag.genre
        tags[:comment] = tag.comment
        properties = fileref.audio_properties
        tags[:bitrate] = properties.bitrate
        tags[:length] = properties.length
      end
    end
    return tags
  end

end
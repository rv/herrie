class MusicDirs

  require './musicdir.rb'

  def initialize db, storage_dir
    @db = db
    @log = Logging.logger.root
    @storage_dir = storage_dir
  end

  def scan
    @log.info ""
    @log.info "---------------------------------------------------"
    @log.info "Scanning storage dir"
    @log.info "#{@storage_dir}"
    @log.info "---------------------------------------------------"

    storagedir_db = @db.add_storagedir(@storage_dir)

    Dir.each_child(@storage_dir) do |dir|
      music_dir = MusicDir.new @db, @storage_dir, storagedir_db, dir
      music_dir.scan
    end

  end

end

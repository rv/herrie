class StorageDirs

  def initialize
    @log = Logging.logger.root
    @storage_dirs = []
  end

  def add dir
    @log.debug "Adding storage dir #{dir}"
    @storage_dirs << dir
  end

  def each
    @storage_dirs.each do |dir|
      yield dir if Dir.exist? dir
    end
  end

end

class Coverfile

  def initialize
    @log = Logging.logger.root
  end

  def scan
    covers = Dir.glob(["*.jpg", "*.png"])
    cover = findcover covers
    @log.info "\u{1f5bd} #{cover}"
    return cover
  end

  def findcover(covers)
    if covers.length == 1
      return covers.first
    else
      low = Set.new covers.map(&:downcase)
      patterns = ['^folder.', '^cover.', '^front.', 'front', 'folder', 'cover']
      patterns.each do |pattern|
        cover = low.grep(/#{pattern}/)
        if not cover.empty?
          return cover.first
        end
      end
    end
    cover = nil
  end

end
